import './assets/main.css'
import 'vue-final-modal/style.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createI18n } from 'vue-i18n'
import { createVfm } from 'vue-final-modal'

import App from './App.vue'
import router from './router'
import { enMessages, frMessages } from './services/i18n'

const i18n = createI18n({
  legacy: false,
  locale: navigator.language,
  fallbackLocale: 'en',
  messages: {
    en: enMessages,
    fr: frMessages,
  },
})
const vfm = createVfm()

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(i18n)
app.use(vfm)

app.mount('#app')
