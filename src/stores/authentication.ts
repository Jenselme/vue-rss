import { ref } from 'vue'
import { defineStore } from 'pinia'
import { login as loginToBackend, isLoggedIn as isLoggedInToBackend } from '@/services/backends'
import { useRouter } from 'vue-router'
import type { AuthenticationData } from '@/services/backends/rss.types'

export const useAuthenticationStore = defineStore('authentication', () => {
  const router = useRouter()

  const values = ref<AuthenticationData>({
    host: '',
    username: '',
    password: '',
    selectedBackend: 'TTRSS',
    token: '',
    isLoggedIn: false,
  })

  const login = (loginValues: AuthenticationData) => {
    values.value = loginValues

    return loginToBackend(loginValues).then(
      (token) => {
        values.value.isLoggedIn = true
        values.value.token = token
      },
      (error) => {
        values.value.isLoggedIn = false
        return Promise.reject(error)
      },
    )
  }

  const logoutAndToGoLoginPage = () => {
    router.push('/').then(() => {
      logout()
    })
  }

  const logout = () => {
    values.value = {
      host: '',
      username: '',
      password: '',
      selectedBackend: 'TTRSS',
      token: '',
      isLoggedIn: false,
    }
  }

  const isLoggedIn = async () => {
    if (!values.value.host) {
      logout()
      return
    }

    const loggedIn = await isLoggedInToBackend(values.value)
    if (loggedIn) {
      values.value.isLoggedIn = true
    } else if (values.value.username && values.value.password) {
      await login(values.value)
    } else {
      logout()
    }
  }

  return { values, login, isLoggedIn, logoutAndToGoLoginPage }
})
