import { defineStore } from 'pinia'
import { ref } from 'vue'

export type Options = {
  fetchDescription?: boolean
}

export const useOptions = defineStore('options', () => {
  const options = ref<Options>({
    fetchDescription: true,
  })

  return { options }
})
