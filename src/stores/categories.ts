import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { listCategories, updateCountersForCategories } from '@/services/backends'
import { useAuthenticationStore } from './authentication'
import { useI18n } from 'vue-i18n'
import type { Category } from '@/services/backends/rss.types'

export const useCategories = defineStore('categories', () => {
  const authenticationStore = useAuthenticationStore()
  const { t } = useI18n()
  const translateCategories = categoriesTranslator(t)
  const rawCategories = ref<Category[]>([])
  const categories = computed(() => translateCategories(rawCategories.value))
  const loading = ref(false)

  const list = async () => {
    loading.value = true
    rawCategories.value = await listCategories(authenticationStore.values)
    loading.value = false
  }

  const updateCounters = async () => {
    rawCategories.value = await updateCountersForCategories(
      authenticationStore.values,
      rawCategories.value,
    )
  }

  return { categories, loading, list, updateCounters }
})

const categoriesTranslator = (t: (id: string) => string) => (categories: Category[]) =>
  categories.map((category) =>
    category.isSpecial ? { ...category, title: t(category.title) } : category,
  )
