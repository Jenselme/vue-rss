import { ref } from 'vue'
import { defineStore } from 'pinia'
import { markArticleAsRead, markArticleAsUnread } from '@/services/backends'
import { useAuthenticationStore } from './authentication'
import { listArticles } from '@/services/backends'
import { useCategories } from './categories'
import { useOptions } from './options'
import type { Article, ArticleId, CategoryId } from '@/services/backends/rss.types'

export const useArticles = defineStore('articles', () => {
  const authStore = useAuthenticationStore()
  const categories = useCategories()
  const options = useOptions()
  const articles = ref<Article[]>([])
  const loading = ref(false)

  const list = async (categoryId: CategoryId | undefined) => {
    if (!categoryId) {
      return
    }

    loading.value = true
    articles.value = await listArticles(
      authStore.values,
      categoryId,
      options.options.fetchDescription,
    )
    loading.value = false
  }

  const markAsRead = async (articleId: ArticleId) => {
    await updateReadStatus({ action: 'read', articleId })
    articles.value = articles.value.map((article) =>
      article.id === articleId ? { ...article, isRead: true } : article,
    )
  }

  type UpdateReadStatusParams = {
    action: 'read' | 'unread'
    articleId: ArticleId
  }

  const updateReadStatus = async ({ action, articleId }: UpdateReadStatusParams) => {
    switch (action) {
      case 'read':
        await markArticleAsRead(authStore.values, articleId)
        break
      case 'unread':
        await markArticleAsUnread(authStore.values, articleId)
        break
    }

    await categories.updateCounters()
  }

  const markAsUnread = async (articleId: ArticleId) => {
    await updateReadStatus({ action: 'unread', articleId })
    articles.value = articles.value.map((article) =>
      article.id === articleId ? { ...article, isRead: false } : article,
    )
  }

  return { articles, loading, list, markAsRead, markAsUnread }
})
