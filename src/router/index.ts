import { useAuthenticationStore } from '@/stores/authentication'
import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login.page-title',
      component: () => import('../views/LoginView.vue'),
    },
    {
      path: '/categories/:categoryId',
      name: 'categories.title',
      component: () => import('../views/CategoriesView.vue'),
    },
  ],
})

router.beforeEach(async (to) => {
  const authenticationStore = useAuthenticationStore()

  if (to.path === '/') {
    return
  }

  await authenticationStore.isLoggedIn()
  if (!authenticationStore.values.isLoggedIn && to.path !== '/') {
    return '/'
  }
})

export default router
