export const frMessages = {
  login: {
    'page-title': 'Connexion | Svelte RSS',
    host: 'Hôte',
    username: 'Nom d’utilisateur',
    password: 'Mot de passe',
    'server-type': 'Type de serveur',
    connect: 'Se connecter',
    'error-title': 'Erreur',
    logout: 'Déconnexion',
  },
  categories: {
    title: "Catégorie ''{name}''",
    special: {
      all: 'Tous les articles',
      unread: 'Articles non lus',
      favorites: 'Articles favoris',
    },
  },
  article: {
    'mark-as-read': 'Marquer comme lu',
    'mark-as-unread': 'Marquer comme non lu',
    open: 'Ouvrir',
  },
  articles: {
    'no-articles': 'Aucun article à afficher.',
  },
  options: {
    'fetch-descriptions': 'Afficher les descriptions',
    'read-on-scroll': 'Read on scroll',
  },
  menu: {
    close: 'Fermer',
  },
}

export const enMessages = {
  login: {
    'page-title': 'Login | Svelte RSS',
    host: 'Host',
    username: 'Username',
    password: 'Password',
    'server-type': 'Server type',
    connect: 'Connect',
    'error-title': 'Error',
    logout: 'Logout',
  },
  categories: {
    title: "Category ''{name}''",
    special: {
      all: 'All articles',
      unread: 'Unread articles',
      favorites: 'Favorite articles',
    },
  },
  article: {
    'mark-as-read': 'Mark as read',
    'mark-as-unread': 'Mark as unread',
    open: 'Open',
  },
  articles: {
    'no-articles': 'No article to display.',
  },
  options: {
    'fetch-descriptions': 'Display descriptions',
    'read-on-scroll': 'Read on scroll',
  },
  menu: {
    close: 'Close',
  },
}
