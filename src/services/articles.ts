const stripHTMLTags = (html: string): string => {
  const doc = new DOMParser().parseFromString(html, 'text/html')
  return doc.body.textContent || ''
}

const maxLengthDescription = 500
const limitDescriptionLength = (description: string): string => {
  if (description.length > maxLengthDescription) {
    return description.substring(0, maxLengthDescription) + '…'
  }

  return description
}

export const sanitizeDescription = (rawDescription: string | undefined): string => {
  if (!rawDescription) {
    return ''
  }

  const cleanedString = stripHTMLTags(rawDescription)
  return limitDescriptionLength(cleanedString)
}
