export type ArticleId = number | string
type FeedId = number | string

export interface Article {
  readonly author: string
  readonly feedId: FeedId
  readonly feedTitle: string
  readonly id: ArticleId
  readonly isRead: boolean
  readonly isFavorite: boolean
  readonly link: string
  readonly title: string
  readonly description: string
  readonly updatedAt: Date
}

export type AuthenticationData = {
  host: string
  username: string
  password: string
  selectedBackend: 'TTRSS'
  token: string
  isLoggedIn: boolean
}

export type CategoryId = number | string

export interface Category {
  readonly id: CategoryId
  readonly title: string
  readonly unreadCount: number
  readonly isSpecial: boolean
}
