import type { Article, ArticleId, AuthenticationData, Category, CategoryId } from './rss.types'
import * as ttrss from './ttrss'

export const login = (params: AuthenticationData): Promise<string> => {
  switch (params.selectedBackend) {
    case 'TTRSS':
      return ttrss.login(params)
  }
}

export const listCategories = (authData: AuthenticationData): Promise<Category[]> => {
  switch (authData.selectedBackend) {
    case 'TTRSS':
      return ttrss.listCategories(authData)
  }
}

export const transformCategoryUrlIdToCategoryId = (
  authData: AuthenticationData,
  urlId: string,
): string => {
  switch (authData.selectedBackend) {
    case 'TTRSS':
      return ttrss.transformCategoryUrlIdToCategoryId(urlId)
  }
}

export const isLoggedIn = (authData: AuthenticationData): Promise<boolean> => {
  switch (authData.selectedBackend) {
    case 'TTRSS':
      return ttrss.isLoggedIn(authData)
  }
}

export const listArticles = (
  authData: AuthenticationData,
  categoryId: CategoryId,
  includeDescription: boolean | undefined,
): Promise<Article[]> => {
  switch (authData.selectedBackend) {
    case 'TTRSS':
      return ttrss.listArticles(authData, categoryId, includeDescription)
  }
}

export const markArticleAsRead = (
  authData: AuthenticationData,
  articleId: ArticleId,
): Promise<void> => {
  switch (authData.selectedBackend) {
    case 'TTRSS':
      return ttrss.markArticleAsRead(authData, articleId)
  }
}

export const markArticleAsUnread = (
  authData: AuthenticationData,
  articleId: ArticleId,
): Promise<void> => {
  switch (authData.selectedBackend) {
    case 'TTRSS':
      return ttrss.markArticleAsUnread(authData, articleId)
  }
}

export const updateCountersForCategories = (
  authData: AuthenticationData,
  categories: Category[],
): Promise<Category[]> => {
  switch (authData.selectedBackend) {
    case 'TTRSS':
      return ttrss
        .listCounters(authData)
        .then((counters) => ttrss.updateCategoriesWithCounter(categories, counters))
  }
}
